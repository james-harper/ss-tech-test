import { useState, useRef } from 'react';
import Head from "next/head";
import { Button } from "components/elements";
import { EmptyDays } from "components/parents-evenings";
import Form from "components/parents-evenings/form";
import Day from "../components/parents-evenings/day";
import { PlusIcon } from "components/icons";

export default function IndexPage() {
  const counter = useRef(1);
  const [days, setDays] = useState([{ id: counter.current }]);
  const addDay = () => setDays([...days, {id: ++counter.current}]);
  const removeDay = (id) => setDays(days.filter(day => day.id !== id));

  return (
    <div>
      <Head>
        <title>School Spider Tech Test</title>
      </Head>
      <div className="py-20">
        <div className="max-w-7xl mx-auto">
          <div className="text-center text-gray-600 pb-8">
            <h1 className="text-5xl text-center">School Spider Tech Test</h1>
            <p>
              Recreate the parents evening day generator demonstrated in the
              interview.
            </p>
          </div>
          <div className="grid grid-flow-row gap-4">
            <Form />
              {days.length === 0 && (
                <EmptyDays handleAddDay={addDay} />
              )}

              {days.length > 0 && (
                <>
                  {days.map(day => {
                    return <Day key={day.id} id={day.id} removeDay={() => removeDay(day.id)} />;
                  })}
                  <div className="fixed bottom-0 inset-x-0 p-4 sm:static flex items-center justify-center">
                    <Button onClick={addDay}>
                      <PlusIcon className="w-5 h-5 -ml-2 mr-1" />
                      Add another day
                    </Button>
                  </div>
                </>
              )}

          </div>
        </div>
      </div>
    </div>
  );
}
