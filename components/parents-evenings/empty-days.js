import React from "react";
import { Button } from "components/elements";
import PropTypes from "prop-types";
import { PlusIcon } from "components/icons";

const EmptyDays = ({ handleAddDay }) => (
  <div className="mt-8 border-dashed border-4 border-cool-gray-300 rounded">
    <div className="my-8 text-center space-y-4 text-cool-gray-500">
      <div>You have no days yet.</div>
      <Button color="blue" onClick={handleAddDay}>
        <PlusIcon className="w-5 h-5 -ml-2 mr-1" />
        Add your first day
      </Button>
    </div>
  </div>
);

EmptyDays.propTypes = {
  handleAddDay: PropTypes.func.isRequired,
};

export default EmptyDays;
