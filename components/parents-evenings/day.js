import React, { useState, forwardRef } from "react";
import addMinutes from 'date-fns/addMinutes';
import isAfter from 'date-fns/isAfter'
import { Button, Card } from "components/elements";
import Slot from "./slot";
import { TrashIcon } from "components/icons";

const Day = forwardRef((props) => {
  const [start, setStart] = useState("10:00");
  const [count, setCount] = useState(5);
  const [duration, setDuration] = useState(15);
  const [slots, setSlots] = useState([]);

  const createSlots = () => {
    const newslots = [ ...Array(count).keys() ].map( i => {
      // Bit of a hacky way of creating date object from start time
      const today = new Date();
      const [hour,min] = start.split(':');
      today.setHours(hour);
      today.setMinutes(min);

      const index = i + 1;
      return {
        id: index,
        start: addMinutes(today, index * duration),
        duration,
        type: 'slot'
      }
    });

    setSlots(newslots)
  };

  const changeType = (type) => (id) => {
    const newSlots = slots.map(slot => {
      if (slot.id === id) {
        slot.type = type;
      }

      return slot;
    });

    setSlots(newSlots);
  };

  const deleteSlot = (id) => {
    const newSlots = slots.filter(slot => slot.id !== id);
    setSlots(newSlots);
  };

  const changeDuration = (id, duration) => {
    const changedSlot = slots.find(slot => slot.id === id);
    const originalDuration = changedSlot.duration;
    const newSlots = slots.map(slot => {
      if (slot.id === id) {
        slot.duration = duration;
      }

      if (changedSlot && isAfter(slot.start, changedSlot.start)) {
        slot.start = addMinutes(slot.start, duration - originalDuration);
      }

      return slot;
    });

    setSlots(newSlots);
  }

  return (
    <Card className="p-8 space-y-4">
      <div className="grid grid-cols-4 md:flex gap-4 items-end">
        <div className="space-y-2">
          <label htmlFor="date" className="block">
            Start Time & Date
          </label>
          <input className="form-input" placeholder="Date" name="date" value={start} onChange={e => setStart(e.target.value)} />
        </div>
        <div className="space-y-2">
          <label htmlFor="slotCount" className="block">
            Slot Count
          </label>
          <input className="form-input" placeholder="Slots" name="slotCount" value={count} onChange={e => setCount(Number(e.target.value))} />
        </div>
        <div className="space-y-2">
          <label htmlFor="duration" className="label">
            Slot Duration (mins)
          </label>
          <div className="grid gap-2 grid-cols-6 grid-auto-col">
            {[5, 10, 15, 20, 25, 30].map(time => (
              <span key={time}>
                <Button
                  size="md"
                  key={time}
                  color={time === duration ? "blue" : "white"}
                  onClick={() => {
                    setDuration(time);
                  }}
                  className="w-full justify-center"
                  spanClass="w-full"
                >
                  {time}
                </Button>
              </span>
            ))}
          </div>
        </div>
        <div className="">
          <Button onClick={createSlots}>Create Slots</Button>
        </div>
        <div className="ml-auto">
          <Button color="white" size="sm" onClick={props.removeDay}>
            <TrashIcon className="w-5 h-5 text-red-500" />
          </Button>
        </div>
      </div>
      <div className="flex flex-wrap gap-x-2 gap-y-4 py-4">
        {slots.map(slot => <Slot
          key={slot.id}
          slot={slot}
          makeSlot={changeType('slot')}
          makeBreak={changeType('break')}
          deleteSlot={deleteSlot}
          changeDuration={changeDuration}
        />)}
      </div>
    </Card>
  );
});

export default Day;
